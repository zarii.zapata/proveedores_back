import { Component, OnInit } from '@angular/core';
import { ProviderService } from '../../../services/provider.service';
import { Provider } from '../../../services/provider';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  providers: Provider[];
  constructor(
    private providerService: ProviderService
  ) { }

  ngOnInit() {
    this.providerService.getProviders().subscribe(
      provedores => this.providers = provedores
    );
  }


  delete(proveedor: Provider): void {
    Swal.fire({
      title: 'Esta Seguro de Editar este Proveedor?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.value) {
        if (result.value) {
          this.providerService.delete(proveedor.tp_id).subscribe(
            response => {
              this.providers = this.providers.filter(cli => cli !== proveedor)
              Swal.fire({
                title: 'Proveedor Eliminado!',
                text: `Proveedor ${proveedor.tp_name} eliminado con éxito.`,
                icon: 'success'
              })
            }
          )
        }
      }
    });
  }



}
