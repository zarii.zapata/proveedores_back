import { Component, OnInit } from '@angular/core';
import { ProviderService } from '../../../services/provider.service';
import { Provider } from '../../../services/provider';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  private proveedor: Provider = new Provider();
  private titulo: string = "Actualizar Proveedor";

  constructor(
    private providerService: ProviderService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.cargarProvider()
  }

  cargarProvider(): void {
    this.activatedRoute.params.subscribe(params => {
      let id = params['id']
      this.providerService.getProvider(id).subscribe(
        (proveedor) => this.proveedor = proveedor
      )
    })
  }

  updateProvider(): void {
    swal.fire({
      title: 'Esta Seguro de Editar este Proveedor?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.value) {
        this.providerService.updateProvider(this.proveedor)
          .subscribe(resp => {
            console.log(resp['status']);
            console.log(resp['error']);
            if (resp['status'] == false) {
              switch (resp['error']) {
                case "1":
                  swal.fire({
                    title: 'Error al editar, hoy es domingo!',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case "2":
                  swal.fire({
                    title: 'Error al editar, el nombre palíndromo.',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case '3':
                  swal.fire({
                    title: 'Error al editar, la dirección tiene más de 3 vocales.',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case '4':
                  swal.fire({
                    title: 'Error al editar, la dirección no cuenta con el prefijo CL',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case '5':
                  swal.fire({
                    title: 'Error de Base de Datos',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
              }
            } else {
              swal.fire({
                title: 'Edición de proveedor exitosa!',
                icon: 'success',
                showConfirmButton: false
              });

              this.router.navigate(['/list'])
            }            
          });
      }
    });

  }

  validarSiNumero(numero) {
    if (!/^([0-9])*$/.test(numero))
      swal.fire({
        title: 'Valor Ingresado no es un numero',
        icon: 'warning'
      });
  }

}
