import { Component, OnInit } from '@angular/core';
import { ProviderService } from '../../../services/provider.service';
import { Provider } from '../../../services/provider';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  private proveedor: Provider = new Provider();
  private titulo: string = "Crear Proveedor";

  constructor(
    private providerService: ProviderService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  create(): void {
    swal.fire({
      title: 'Esta Seguro de Guardar este Proveedor?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
    }).then((result) => {
      if (result.value) {
        this.providerService.create(this.proveedor)
          .subscribe(resp => {
            console.log(resp['status']);
            console.log(resp['error']);
            if (resp['status'] == false) {
              switch (resp['error']) {
                case "1":
                  swal.fire({
                    title: 'Error al crear, hoy es domingo!',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case "2":
                  swal.fire({
                    title: 'Error al crear, el nombre palíndromo.',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case '3':
                  swal.fire({
                    title: 'Error al crear, la dirección tiene más de 3 vocales.',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case '4':
                  swal.fire({
                    title: 'Error al crear, la dirección no cuenta con el prefijo CL',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
                case '5':
                  swal.fire({
                    title: 'Error de Base de Datos',
                    icon: 'warning',
                    showConfirmButton: false
                  });
                  break;
              }
            } else {
              swal.fire({
                title: 'Registro de proveedor exitoso!',
                icon: 'success',
                showConfirmButton: false
              });

              this.router.navigate(['/list'])
            }            
          });
      }
    });
  }


  validarSiNumero(numero) {
    if (!/^([0-9])*$/.test(numero))
      swal.fire({
        title: 'Valor Ingresado no es un numero',
        icon: 'warning'
      });
  }

  keyPress(event: any) {
    const pattern = /[0-9]/;
    const inputChar = String.fromCharCode(event.charCode);

    if (!pattern.test(inputChar)) {
      // invalid character, prevent input
      swal.fire({
        title: 'Valor Ingresado no es un número',
        icon: 'warning',
        showConfirmButton: false
      });

      event.preventDefault();
    }
  }

}
