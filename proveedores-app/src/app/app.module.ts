import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { FooterComponent } from './footer/footer.component';

import { ListComponent } from './pages/proveedor/list/list.component';
import { CreateComponent } from './pages/proveedor/create/create.component';
import { EditComponent } from './pages/proveedor/edit/edit.component';
import { ProviderService } from './services/provider.service';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  { path: '', redirectTo: '/list', pathMatch: 'full' },
  { path: 'list', component: ListComponent },
  { path: 'register', component: CreateComponent },
  { path: 'edit/:id', component: EditComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FooterComponent,
    ListComponent,
    CreateComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    ProviderService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
