import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Provider } from './provider';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  private url: string = 'http://localhost:8080/api';
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' })

  constructor(
    private http: HttpClient,
  ) { }

  getProviders(): Observable<Provider[]> {
    return this.http.get(this.url + "/list").pipe(
      map(response => response as Provider[])
    );
  }

  getProvider(id): Observable<Provider> {
    return this.http.get<Provider>(`${this.url}/show/${id}`)
  }

  updateProvider(provider: Provider): Observable<Provider> {
    return this.http.put<Provider>(`${this.url}/update/${provider.tp_id}`, provider)
  }

  create(provider: Provider): Observable<Provider> {
    return this.http.post<Provider>(`${this.url}/save`, provider, { headers: this.httpHeaders })
  }

  delete(id: number): Observable<Provider> {
    return this.http.delete<Provider>(`${this.url}/delete/${id}`, { headers: this.httpHeaders })
  }
}
