package com.project.springboot.app;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.project.springboot.app.businessLogic.Domingos;
import com.project.springboot.app.businessLogic.Palindromo;
import com.project.springboot.app.businessLogic.Prefijo;

@SpringBootTest
class SpringBootDataProveedoresApplicationTests {

	@Test
	void contextLoads() {
	}

	@Mock
	static Palindromo palindromo;

	@Test
	void comprobarPalindromo() throws Exception {

		String nombreProveedor = "AAAAAAA";
		boolean pal = Palindromo.comprobarPalindromo(nombreProveedor);
		Assertions.assertTrue(pal);
	}

	@Test
	void comprobarPalindromo_NO() throws Exception {

		String nombreProveedor = "SURAMERICANA";
		boolean pal = Palindromo.comprobarPalindromo(nombreProveedor);
		Assertions.assertFalse(pal);
	}
	
	@Test
	void esDomingo() throws Exception
	{
		int dia=6;
		boolean esdomingo=Domingos.comprobarDomingo(dia);
		Assertions.assertFalse(esdomingo);
	}
	
	@Test
	void prefijo_cl() throws Exception {
		String direccion="CL 34 # 56 32";
		boolean conprefijo=false;;
		conprefijo=Prefijo
				.comprobarPrefijo(direccion);
		Assertions.assertFalse(conprefijo);
	}
	@Test
	void prefijo_no() throws Exception {
		String direccion="CR 34 # 56 32";
		boolean sinprefijo=false;
		sinprefijo=Prefijo.comprobarPrefijo(direccion);
		Assertions.assertTrue(sinprefijo);
	}
	
	@Test
	void esDomingo_si() throws Exception
	{
		int dia=7;
		boolean esdomingo=Domingos.comprobarDomingo(dia);
		Assertions.assertTrue(esdomingo);
	}

	@Test
	void tresvocales_si() throws Exception {
		String nombreProveedor="BANCOLOMBIA";
		Boolean bol3Vocales=Prefijo.contarVocales(nombreProveedor);
		Assertions.assertTrue(bol3Vocales);
	}
	@Test
	void tresvocales_no() throws Exception {
		String nombreProveedor="FLASH TO";
		Boolean bol3Vocales=Prefijo.contarVocales(nombreProveedor);
		Assertions.assertFalse(bol3Vocales);
	}
}
