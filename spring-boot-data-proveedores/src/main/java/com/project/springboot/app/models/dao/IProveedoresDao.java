package com.project.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.project.springboot.app.models.entity.Proveedores;

public interface IProveedoresDao extends CrudRepository<Proveedores, Long> {

	
}
