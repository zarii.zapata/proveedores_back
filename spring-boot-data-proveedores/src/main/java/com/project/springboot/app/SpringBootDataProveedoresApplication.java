package com.project.springboot.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataProveedoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataProveedoresApplication.class, args);
	}

}
