package com.project.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.project.springboot.app.models.dao.IProveedoresDao;
import com.project.springboot.app.models.entity.Proveedores;

@Service
public class ProveedoresServiceImpl implements IProveedoresService{

	@Autowired
	private IProveedoresDao proveedoresDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Proveedores> findAll() {
		return (List<Proveedores>) proveedoresDao.findAll();
	}
	
	@Override
	@Transactional(readOnly = true)
	public Proveedores findById(Long id) {
		// TODO Auto-generated method stub
		return proveedoresDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public Proveedores save(Proveedores proveedores) {
		return proveedoresDao.save(proveedores);
	}
	
	public void delete(Long idProveedor) {
		proveedoresDao.deleteById(idProveedor);
	}
}
