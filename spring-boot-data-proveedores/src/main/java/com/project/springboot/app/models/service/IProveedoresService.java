package com.project.springboot.app.models.service;

import java.util.List;
import com.project.springboot.app.models.entity.Proveedores;

public interface IProveedoresService  {

	public List <Proveedores> findAll();
	
	public Proveedores findById(Long id);
	
	public Proveedores save(Proveedores proveedores);

	public void delete(Long id);
}
