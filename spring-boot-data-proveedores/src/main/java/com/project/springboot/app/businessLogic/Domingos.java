package com.project.springboot.app.businessLogic;

import java.util.Calendar;

public class Domingos {
	
	public static boolean comprobarDomingo (int dia) {		
		boolean domingo;	
	    
	    if(dia == Calendar.SUNDAY){
	    	domingo = true;
	    } else {
	    	domingo = false;
	    }	    
	    
		return domingo;
	}
	

}
