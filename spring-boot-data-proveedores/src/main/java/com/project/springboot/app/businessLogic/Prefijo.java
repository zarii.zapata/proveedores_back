package com.project.springboot.app.businessLogic;

public class Prefijo {

	public static boolean contarVocales(String cadena) {
		int contador = 0;
		boolean validar = false;
		cadena=cadena.toLowerCase();
		for (int x = 0; x < cadena.length(); x++) {
			if ((cadena.charAt(x) == 'a') || (cadena.charAt(x) == 'e') || (cadena.charAt(x) == 'i')
					|| (cadena.charAt(x) == 'o') || (cadena.charAt(x) == 'u')) {
				contador++;
				
				
			}
		}
		if(contador >= 3) {
			validar = true;
		}
		
		return validar;
	}
	
	
	public static boolean comprobarPrefijo(String cadena) {
		boolean validar = false;
		
		String sSubCadena = cadena.substring(0,2).toUpperCase();
		
		if(!sSubCadena.equals("CL")) {
			validar = true;
		}
		
		return validar;
	}

}
