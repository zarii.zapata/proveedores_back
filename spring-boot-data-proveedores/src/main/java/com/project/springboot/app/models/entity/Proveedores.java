package com.project.springboot.app.models.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Entity
@Table(name = "t_proveedores")
public class Proveedores implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long tp_id;
	
	@NotEmpty
	@Size(min = 5, message="El Nombre debe tener 5 caracteres minimo")
	private String tp_name;
	
	@NotEmpty
	@Size(min = 7, max= 7,  message="El telefono debe tener 7 Caracteres")
	private String tp_phone;
	
	@NotEmpty
	private String tp_address;

	@Temporal(TemporalType.DATE)
	private Date tp_date;

	@PrePersist
	public void prePersist() {
		tp_date = new Date();
	}

	public Long getTp_id() {
		return tp_id;
	}

	public void setTp_id(Long tp_id) {
		this.tp_id = tp_id;
	}

	public String getTp_name() {
		return tp_name;
	}

	public void setTp_name(String tp_name) {
		this.tp_name = tp_name;
	}

	public String getTp_phone() {
		return tp_phone;
	}

	public void setTp_phone(String tp_phone) {
		this.tp_phone = tp_phone;
	}

	public Date getTp_date() {
		return tp_date;
	}

	public void setTp_date(Date tp_date) {
		this.tp_date = tp_date;
	}

	public String getTp_address() {
		return tp_address;
	}

	public void setTp_address(String tp_address) {
		this.tp_address = tp_address;
	}

}
