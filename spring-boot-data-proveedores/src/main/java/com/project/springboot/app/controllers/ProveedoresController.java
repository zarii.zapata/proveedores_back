package com.project.springboot.app.controllers;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.project.springboot.app.businessLogic.Domingos;
import com.project.springboot.app.businessLogic.Palindromo;
import com.project.springboot.app.businessLogic.Prefijo;
import com.project.springboot.app.models.entity.Proveedores;
import com.project.springboot.app.models.service.IProveedoresService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class ProveedoresController {
	@Autowired
	private IProveedoresService proveedoresService;

	@GetMapping("/list")
	public List<Proveedores> index() {
		return proveedoresService.findAll();
	}

	@GetMapping("/show/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Proveedores show(@PathVariable Long id) {
		Proveedores response = null;
		response = proveedoresService.findById(id);
		return response;
	}

	@PostMapping("/save")
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<?> save(@Valid @RequestBody Proveedores proveedor) {

		Proveedores proveedorNew = null;
		Map<String, Object> response = new HashMap<>();

		String nombreProveedor = proveedor.getTp_name();
		Calendar fechaSistema = Calendar.getInstance();
		int pruebaDia = fechaSistema.get(Calendar.DAY_OF_WEEK);
		String contarVocales = proveedor.getTp_address();

		boolean palindromo = Palindromo.comprobarPalindromo(nombreProveedor);
		boolean domingo = Domingos.comprobarDomingo(pruebaDia);
		boolean direccion = Prefijo.contarVocales(contarVocales);
		boolean direccionPrefijo = Prefijo.comprobarPrefijo(contarVocales);
		try {
			if(!domingo) {
				if(!palindromo) {
					if(!direccion) {
						if(!direccionPrefijo) {
							proveedor.setTp_date(new Date());
							proveedorNew = proveedoresService.save(proveedor);
							response.put("message", "Proveedor creado en Base de datos");
							response.put("status", true);
						} else {
							response.put("error", "4");
							response.put("message", "Error al realizar insert la direccion no cuenta con el prefijo CL");
							response.put("status", false);
						}
					} else {
						response.put("error", "3");
						response.put("message", "Error al realizar insertar direccion tiene mas de tres  vocales ");
						response.put("status", false);
					}
				} else {
					response.put("error", "2");
					response.put("message", "Error al realizar insert el nombre palindromo");
					response.put("status", false);
				}
			} else {
				response.put("error", "1");
				response.put("message", "Error al realizar insert hoy es domingo");
				response.put("status", false);
			}
		} catch (DataAccessException e) {
			response.put("error", "5");
			response.put("message", "Error al realizar insert en Base de  datos");
			response.put("status", false);
		}

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@PutMapping("/update/{id}")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public ResponseEntity<?> update(@Valid @RequestBody Proveedores proveedor, @PathVariable Long id) {
		
		Map<String, Object> response = new HashMap<>();
		Proveedores proveedorResponse = null;
		
		boolean palindromo = Palindromo.comprobarPalindromo(proveedor.getTp_name());
		boolean direccion = Prefijo.contarVocales(proveedor.getTp_address().toLowerCase());
		boolean direccionPrefijo = Prefijo.comprobarPrefijo(proveedor.getTp_address());

		
		try {
				if(!palindromo) {
					if(!direccion) {
						if(!direccionPrefijo) {
							proveedorResponse = proveedoresService.findById(id);
							proveedorResponse.setTp_address(proveedor.getTp_address());
							proveedorResponse.setTp_name(proveedor.getTp_name());
							proveedorResponse.setTp_phone(proveedor.getTp_phone());
							proveedoresService.save(proveedorResponse);
							response.put("message", "Proveedor actualizado en Base de datos");
							response.put("status", true);
						} else {
							response.put("error", "4");
							response.put("message", "Error al realizar actualizar la direccion no cuenta con el prefijo CL");
							response.put("status", false);
						}
					} else {
						response.put("error", "3");
						response.put("message", "Error al realizar actualizarla direccion tiene mas de tres vocales");
						response.put("status", false);
					}
				} else {
					response.put("error", "2");
					response.put("message", "Error al realizar actualizar el nombre palindromo");
					response.put("status", false);
				}
		} catch (DataAccessException e) {
			response.put("error", "5");
			response.put("message", "Error al realizar update en Base de  datos");
			response.put("status", false);
		}
		
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}

	@DeleteMapping("/delete")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Long id) {
		proveedoresService.delete(id);
	}

}
